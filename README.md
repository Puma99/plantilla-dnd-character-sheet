# Plantilla Dungeon and Dragons Character Sheet

## Getting started

This is a template to start creating D&D books with a style very similar to the official books.

## Installation

Before using this repository you will need [DND-5e-LaTeX-Template](https://github.com/rpgtex/DND-5e-LaTeX-Template) dependency for the project to work.
```bash
sudo git clone https://github.com/rpgtex/DND-5e-LaTeX-Template.git /usr/local/texlive/texmf-local/tex/latex/dnd
```

You may need to update the LaTeX file database by running:

```bash
sudo texhash  
```

We recommend using VS Code with the LaTeX Workshop extension.
